<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fr" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <div><a href="add.html">Registrar</a> </div>
        <table border="2" >
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Numero de casa</th>
                    <th>Direccion</th>
                </tr>
            </thead>
            <tbody>

                <fr:forEach var="reg" items="${list}">
                    <tr>
                        <td>${reg.id}</td>
                        <td>${reg.num_casa}</td>
                        <td>${reg.colonia}</td>
                    </tr>
                </fr:forEach>
            </tbody>
        </table>
    </body>
</html>
