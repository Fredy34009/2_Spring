/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Confs;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author fredy.alfarousam
 */
public class Conexion {

    private DriverManagerDataSource conn;

    public DriverManagerDataSource conecta() {
        conn = new DriverManagerDataSource();
        conn.setDriverClassName("com.mysql.jdbc.Driver");
        conn.setUrl("jdbc:mysql://localhost:3306/vivienda");
        conn.setUsername("root");
        conn.setPassword("root");
        
        System.out.println("Exito en la conexion");
        return conn;
    }

    public void desconecta() {
        try {
            conn.getConnection().close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
