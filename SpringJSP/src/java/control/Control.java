/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Confs.Conexion;
import dao.DCasa;
import models.Casa;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fredy.alfarousam
 */
@Controller
public class Control {
    
    private DCasa dCasa=new DCasa();
    private ModelAndView vi=new ModelAndView();
    
    @RequestMapping("index.html")
    public ModelAndView consultarCasas()
    {
        return dCasa.consultarCasas();
    }
    @RequestMapping(value = "add.html",method = RequestMethod.GET)
    public ModelAndView ingresarCasa()
    {
        vi.addObject(new Casa());
        vi.setViewName("add");
        return vi;
    }
    
    @RequestMapping(value = "add.html", method = RequestMethod.POST)
    public ModelAndView ingresarCliente(Casa casa) {
        return dCasa.insertarCasa(casa);
    }
}
