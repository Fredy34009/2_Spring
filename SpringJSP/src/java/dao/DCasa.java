/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Confs.Conexion;
import java.util.List;
import models.Casa;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fredy.alfarousam
 */
public class DCasa {
    
    private String[] casas={
        "insert into casas(num_casa,colonia) value(?,?)",
        "select id,num_casa,colonia from casas"
    };
    private Casa casa=new Casa();
    private Conexion conn=new Conexion();
    private JdbcTemplate jtem=new JdbcTemplate(conn.conecta());
    private ModelAndView mav=new ModelAndView();
    private List<Casa> list;
    
    //Registra la vivienda
    public ModelAndView insertarCasa(Casa casa)
    {
        this.jtem.update(casas[0],casa.getNum_casa(),casa.getColonia());
        mav.setViewName("redirect:/index.html");
        return mav;
    }
    //consultar la lista de viviendas
    public ModelAndView consultarCasas()
    {
        List lis=this.jtem.queryForList(casas[1]);
        mav.addObject("list",lis);
        mav.setViewName("index");
        return mav;
    }
}
